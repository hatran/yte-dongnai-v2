var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Đa khoa tỉnh Đồng Nai
        Bệnh viên Đa khoa Thống Nhất
        Bệnh viện Đa khoa thành phố Biên Hòa
        Bệnh viện Phổi Đồng Nai
        Bệnh viện Đa khoa khu vực Long Thành
        Bệnh viện Đa khoa khu vực Long Khánh
        Bệnh viện Đa khoa khu vực Định Quán`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Đa khoa tỉnh Đồng Nai
        Bệnh viên Đa khoa Thống Nhất
        Bệnh viện Đa khoa thành phố Biên Hòa
        Bệnh viện Phổi Đồng Nai
        Bệnh viện Đa khoa khu vực Long Thành
        Bệnh viện Đa khoa khu vực Long Khánh
        Bệnh viện Đa khoa khu vực Định Quán`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP Biên Hòa‎
                Cẩm Mỹ‎ 
                Định Quán‎ 
                TP Long Khánh‎ 
                Long Thành‎
                Nhơn Trạch
                Tân Phú‎
                Thống Nhất‎
                Trảng Bom‎ 
                Vĩnh Cửu‎
                Xuân Lộc‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Đa khoa tỉnh Đồng Nai
        Bệnh viên Đa khoa Thống Nhất
        Bệnh viện Đa khoa thành phố Biên Hòa
        Bệnh viện Phổi Đồng Nai
        Bệnh viện Đa khoa khu vực Long Thành
        Bệnh viện Đa khoa khu vực Long Khánh
        Bệnh viện Đa khoa khu vực Định Quán`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Đa khoa tỉnh Đồng Nai
        Bệnh viên Đa khoa Thống Nhất
        Bệnh viện Đa khoa thành phố Biên Hòa
        Bệnh viện Phổi Đồng Nai
        Bệnh viện Đa khoa khu vực Long Thành
        Bệnh viện Đa khoa khu vực Long Khánh
        Bệnh viện Đa khoa khu vực Định Quán`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
